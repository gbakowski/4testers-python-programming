from cars import get_country_of_a_car_brand, get_gas_usage_for_distance


# test dla japonskich aut

def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand("toyota") == "Japan"


def test_get_country_for_a_japanese_car_capitalized():
    assert get_country_of_a_car_brand("Toyota") == "Japan"


# test dla niemieckich aut
def test_get_country_for_a_german_car():
    assert get_country_of_a_car_brand("mercedes") == "Germany"


def test_get_country_for_a_german_car_capitalized():
    assert get_country_of_a_car_brand("BMW") == "Germany"


# test dla francuskich aut
def test_get_country_for_a_french_car():
    assert get_country_of_a_car_brand("peugeot") == "france"


def test_get_country_for_a_french_car_capitalized():
    assert get_country_of_a_car_brand("renault") == "france"


# test dla "unknown" car
def test_get_country_for_a_sweden_car_capitalized():
    assert get_country_of_a_car_brand("volvo") == "Unknown"

# test for a function calculation gas usage per distance

def test_gas_usage_for_zero_distance_driven():
    assert get_gas_usage_for_distance(0, 8.5) == 0

def test_gas_usage_for_100_kilometers_distance_driven():
    assert get_gas_usage_for_distance(100, 8.5) == 8.5

def test_gas_usage_for_negative_distance_driven():
    assert get_gas_usage_for_distance(-50, 0) == 0

def test_gas_usage_for_negative_gas_usage_driven():
    assert get_gas_usage_for_distance(10, -10) == 0