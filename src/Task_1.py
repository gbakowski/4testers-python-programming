# Task 1

import random
import datetime


# # Listy zawierające dane do losowania
# female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
# male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
# surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
# countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
#
# # Przykład jak można losować imię z listy
# random_female_firstname = random.choice(female_fnames)
# print(random_female_firstname)
#
# # Przykład jak można losować wiek z liczb całkowitych od 1 do 65
# random_age = random.randint(1, 45)
# print(random_age)
#
# # Przykładowy wygenerowany pojedynczy słownik
# example_dictionary = {
#     'firstname': 'Kate',
#     'lastname': 'Yu',
#     'email': 'kate.yu@example.com',
#     'age': 23,
#     'country': 'Poland',
#     'adult': True
# }


# Solution for Task 1
# Dictioanry building:

def generate_personal_data_in_the_dictionary(is_male):
    today = datetime.date.today()
    year = today.year

    if is_male:
        personal_type = "male"
        personal_kind = random.choice(['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin'])
    else:
        personal_type = "female"
        personal_kind = random.choice(['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka'])
    personal_age = random.randint(1, 45)
    personal_surname = random.choice(
        ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina'])
    country_of_origin = random.choice(['Poland', 'United Kingdom', 'Germany', 'France', 'Other'])
    personal_email = ({"email: " f"{personal_kind.lower()}.{personal_surname.lower()}@example.com"})
    birth_year = (year - personal_age)
    for personal in list_of_personal_data:
        if personal_age >= 18:
            personal.update({"maturity:": True})
        else:
            personal.update({"maturity:": False})
    return {
        "kind": personal_kind,
        "type": personal_type,
        "age": personal_age,
        "surname": personal_surname,
        "country": country_of_origin,
        "email": personal_email,
        "maturity": personal_age >= 18,
        "birth_of_year": birth_year

    }


# Generate 10 dictionaries, half of them male, half female.
if __name__ == '__main__':
    list_of_personal_data = []
    for i in range(5):
        # if i % 2 == 0:
        list_of_personal_data.append(generate_personal_data_in_the_dictionary(True))
        # else:
        list_of_personal_data.append(generate_personal_data_in_the_dictionary(False))

    for personal in list_of_personal_data:
        print(
            f"Hi! I'am {personal['kind']} {personal['surname']}, I come from {personal['country']} and I was born in {personal['birth_of_year']}.")
