# Exercise 1 Suma temperatur w danym miesiącu
import uuid


def get_list_element_sum(list_with_numbers):
    return sum(list_with_numbers)


def get_average_of_two_numbers(list_with_numbers):
    return sum(list_with_numbers) / len(list_with_numbers)


# Exercise 2 - napisz funkcję która podnosi liczbę do kwadratu

def get_number_squared(digit_to_squared):
    return digit_to_squared ** 2  # potęgowanie (**)


number = 0

power_result = get_number_squared(number)
print(power_result)

number = 16

power_result = get_number_squared(number)
print(power_result)

number = 2.55  # określanie wartości zmiennej która zostanie użyta w naszej funkcji

floating_number_squared = get_number_squared(number)
power_result = "{:.2f}".format(
    power_result)  # formatowanie wyniku "Pwoer results" do liczby po przecinku np. 2.0 / pod power results może być cokolwiek to tylko trzyma nasz kod
print(floating_number_squared)

negative_result = get_number_squared(
    -3)  # (-3) prostsza metoda bez tworzenia nowej zmiennej a po prostu dodania jej do funkcji printowania w ().
print(negative_result)

print(get_number_squared(0), get_number_squared(16), get_number_squared(2.55),
      sep="\t")  # inny najprostszy sposób wyprintowania wszystkich zmiennych w jednej linijce


# Exercise 3 - napisz funkcję, która zwraca objętośc prostopadłościanu o bokach a,b,c / Wykorzystując zdefiniowaną funkcję, oblicz bjętośc prostopadłościanu o wymiarach 3x5x7

def get_cuboid_volume(a, b, c):  # tutaj mamy dobry przykład jak wygląda funckja: jest zbudowana z określenia nazwy (get_cuboid_volume) + wartości, które mają pomóc ją określić (a, b ,c)
    return a * b * c


print(get_cuboid_volume(3, 5, 7))  # skrót do obliczania/printowania zmiennnych w jednej linijce

# Exercise 4


if __name__ == "__main__":
    january = [-4, 1.0, -7, 2]
    february = [-13, -14, 23, 2]

    # print(f'January: {get_list_element_sum(temperatures_in_january)}')
    # print(f'February: {get_list_element_sum(temperatures_in_february)}')

    # average_temperature = get_average_of_two_numbers(get_average_of_two_numbers(january),
    #                 get_average_of_two_numbers(february))
    # print("Average Temperature is:", average_temperature)


def get_user_credtentials(email):
    get_user_credentials_dict = {"email": email, "password": str(uuid.uuid4())}
    return get_user_credentials_dict


def run_exercise_2():
    print(f'User credetnials: {get_user_credtentials("user@example.com")}')
    print(f'User credetnials: {get_user_credtentials("user@example.pl")}')


if __name__ == '__main__':
    # run_exercise_1()
    run_exercise_2()


# Exercise 5
def print_gamer_descriptios(gamer_dictionary):
    print(
        f"The player {gamer_dictionary['nick']} is type of {gamer_dictionary['type']} and has {gamer_dictionary['exp_points']}")


def run_exercise_3():
    player = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    print_gamer_descriptios(player)


# Exercise 6: Napisz funkcję która konwertuje stopnie Celsjusza na Fahrenheity. Ile Fahrenheitów to 20 stopni C?
def convert_celsius_to_fahrenheit(temperatures_in_celsius):  # prosta funkcja jak juz w return określić wartośc względem której zmieniamy Farenheity na Celsiuse
    return temperatures_in_celsius * 9 / 5 + 32


farenheit = convert_celsius_to_fahrenheit(20)  # Tu (20) określilismy wartośc farenheitów, która będzie przeliczona na Celsiuse.
print(farenheit)

#     temps_fahrenheit = []  # Od tego momentu i poniżej budowana jest lista która konwertuje liste wartości F na listę wartości  C.
#     for temp in temperatures_in_celsius:
#         temps_fahrenheit.append(round(temp * 9 / 5 + 32, 2))
#     return temps_fahrenheit
#
#
# if __name__ == '__main__':
#     temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
#     print(convert_celsius_to_fahrenheit(temps_celsius))
#     # convert_celsius_to_fahrenheit(100)
