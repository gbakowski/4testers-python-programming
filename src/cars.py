def get_country_of_a_car_brand(car_brand):
    car_brand_lowercase = car_brand.lower()
    if car_brand_lowercase in ("toyota", "Mazda", "Suzuki", "Subaru"):
        return "Japan"
    elif car_brand_lowercase in ("bmw", "mercedes", "audi", "volkswagen"):
        return "Germany"
    elif car_brand_lowercase in ("renault", "peugeot"):
        return "france"
    else:
        return "Unknown"

# Pisanie testów w Python do tego powyżej pod test_cars.py


# metoda ile spaliłem litrów paliwa - przyjmuje dystans i spalanie na 100km
def get_gas_usage_for_distance(distance_in_kilometers, average_gas_usage_per_100_km):
    if distance_in_kilometers < 0 or average_gas_usage_per_100_km < 0:
        return 0
    return distance_in_kilometers / 100 * average_gas_usage_per_100_km
