my_name = "Grzegorz"
my_age = 35
my_email = "gbakowski@gmail.com"

print(my_name)
print(my_age)
print(my_email)
print(my_name, my_age, my_email)

# The description of my friend - task 1
my_friend_name = "Hubert"
my_friend_age = 30
my_friend_has_animals = 0
my_friend_has_driving_license = True
my_friend_friendship_time = 10.5

print(my_friend_name, my_friend_age, my_friend_has_animals, my_friend_has_driving_license, my_friend_friendship_time,
      sep="\t")  # ten sep pozwala oddzielić kolejne nasze rzeczy różnymi separatorami
print("Friend's name:", my_friend_name, "\nFriend's age:", my_friend_age)


def print_hello():
    print('Hello')


print_hello()
