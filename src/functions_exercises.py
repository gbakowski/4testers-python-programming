def upper_word(word):  # funkcja to umożliwia pisanie wszystkiego w duzych literach
    return word.upper()


big_dog = upper_word('dog')
print(big_dog)
print(upper_word("tree"))


# def add_two_number(a, b):
# return a + b

def print_a_car_brand_name():
    print("Honda")


print_a_car_brand_name()


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


print_given_number_multiplied_by_3(10)


def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2


area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)


def calculate_area_of_triangle(bottom, height):
    return 0.5 * bottom * height


area_a_little_triangle = calculate_area_of_triangle(10, 5)
print(area_a_little_triangle)

# Checking  what is return from a function by default
result_of_print_function = print_a_car_brand_name()
print(result_of_print_function)


# Exercise 1 - jak stworzyć funkcję, która wyświetli liczbę do kwadratu
def get_number_squared(num):
    return num ** 2


zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)


# Exercise 2 - funkcja do obliczania prosopadłościanu
def get_cuboid_volume(a, b, c):
    return a * b * c


print(get_cuboid_volume(3, 5, 7))

# Exercise 3 - konwenter temp Celcjusza na Faranhaita

# def convert_temperature_in_celcius_to_farenheit(temp_in_celcius):
# return temp_in_celcius * 9 / 5 + 32

# farenhait = 68
# print (f"Celsius =")
