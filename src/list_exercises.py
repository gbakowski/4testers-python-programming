movies = ["Gladiator", "Forest Gump", "Interstellar", "James Bond", "Avengers"]

last_movie = movies[-1]
print(f"My last movies: {last_movie}")
# tak dodajemy element do listy, który ląduje na końcu listy
movies.append("Star Trek")
print(f"My new last movies: {movies[-1]}")

# tak wyświetlamy długośc listy:
print(f" The length of movies list is {len(movies)}")

movies.insert(0, "Pinokio")
movies.remove("Forest Gump")

# Tak zastępujemy elementy w liście
movies[-1] = "Dune (1980)"
print(movies)

emails = ["a@example.com", "b@example.com"]

print(emails[0])
print(emails[-1])
emails.append("cde@example.com")
print(f"To jest długość listy: {len(emails)}")

# Dictionary - słownik
friend = {
    "name": "Artur",
    "age": 28,
    "hobbies": ["cycling", "trekking"]
}
print(friend["age"])
friend["city"] = "Wrocław"
friend["age"] = 39
del friend["city"]  # Usuwanie elemntu z listy
print(friend["hobbies"][-1])
friend["hobbies"].append("climbing")
print(len(friend))
