def print_temperatrue_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    elif temperature_in_celsius > 0:  # To jest funkcja pośrednia (dodatkowa)
        print("It's quite OK")
    else:
        print("It's getting cold")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


# return True if age >= 18 else False

if __name__ == "__main__":
    temperature_in_celsius = -100
    print_temperatrue_description(temperature_in_celsius)
    age_kate = 17
    age_tom = 18
    age_marta = 21
    print("Kate", is_person_an_adult(age_kate))
    print("Tom", is_person_an_adult(age_tom))
    print("Marta", is_person_an_adult(age_marta))


# == - równe
# >= - mniejsze bądź równe
# != - różne

a = -1
if a > 0:
    print("a is positive")
else:
    print("a is not positive")


def print_word_lenght_descriptions(name):
    if len(name) > 5:
        print(f"The name {name} is longer than 5 characters")
    else:
        print(f"The name {name} is 5 characters or shorter")

def print_temperature_and_pressure_conditions(celcius, pressure):
    if (celcius == 0) & (pressure == 1013):
        return True
    else:
       return False

def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2

if __name__ == '__main__':

    long_name = "Grzegorz"
    short_name = "Ada"

    print_word_lenght_descriptions(long_name)
    print_word_lenght_descriptions(short_name)


    print(print_temperature_and_pressure_conditions(0, 1013))
    print(print_temperature_and_pressure_conditions(1, 1014))
    print(print_temperature_and_pressure_conditions(0, 1014))
    print(print_temperature_and_pressure_conditions(1, 1014))

    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))