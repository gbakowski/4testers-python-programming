# Lists
shopping_list = ["oranges", "water", "oil", "chicken", "potatoes"]
print(shopping_list[0])  # drukowanie elementy z listy z indeksem [0]
print(shopping_list[-1])
print(shopping_list[-2])

shopping_list.append("lemons")  # dodawnie elementu do listy, który zawsze wpada na koniec listy
print(shopping_list)

number_of_items_to_buy = len(
    shopping_list)  # w ten sposób tworzymy zmienną dla listy i możemy wyświetlić ile elementów się tam znajduje
print(number_of_items_to_buy)

first_three_shopping_items = shopping_list[
                             0:3]  # w ten sposób wyciągamy kawałek danych z listy podajemu początek oraz indeks na prawo od ostaenigo miejsca które checmy wyciągnąć
print(first_three_shopping_items)

# Dictionares - słowniki mają klucze i wartości (zarówno liczby i stringi (najczęsciej)

animal = {
    "name": "Bureek",
    "kind": "dog",
    "age": 7,
    "male": True
}
dog_age = animal["age"]
print("Dog age:", dog_age)
dog_name = animal["name"]
print("Dog name: ", dog_name)

animal["age"] = 10  # tak zmieniamy wartośc w slowniku
animal["owner"] = "Staszek"  # tak dodajemy wartości do słownika
print(animal)

#New task on lesson from monday

if __name__ == '__main__':

    addresses = [
    {
        "city": "Warsaw",
        "street": "Zielona",
        "number": "5",
        "post code": "04-684"
    },
    {
        "city": "Gdansk",
        "street": "Plazowa",
        "number": "13",
        "post code": "05-564"
    },
    {
        "city": "Krakow",
        "street": "Smocza",
        "number": "78",
        "post code": "13-683"
    }
]
print(addresses[-1]["post code"])
print(addresses[1]["city"])
addresses[0]["street"] = "Bialobrzeska"
print(addresses)
