my_name = "Grzegorz"
favourite_movie = "Star Wars"
favourite_actor = "Clint Eastwood"

# to (f) -formatowanie przed stringiem dodajemy bo pozwala nam napisać cały tekst w jednym stringu i dodać pomiędzy wart. zmiennych.
bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

calculation_example = f"sum of 2 and 4 is {2 + 4}."
print(calculation_example)

# Exercise 1 - Print welcome message to a person in city
your_name = "Michal"
your_city = "Torun"

bio = f"Witaj {your_name} Milo Cie widziec w naszym miescie: {your_city}"
print(bio)

# Tu jest przy pomocy skróconej funkcji
name1 = "Michał"
city1 = "Toruń"

welcome_text = f"Witaj {name1}! Miło cię widzieć w naszym mieście: {city1}!"
print(welcome_text)


# A tu typowa funkcja
def print_hello_message(name, city):
    name_capitalized = name.capitalize() # to .capitalize() to metoda która zmusza do wyświetlania zmiennej zawsze z dużej pierwszej litery - trzeba pamiętać o . i () bo to wywołuje zmienną.
    city_capitalized = city.capitalize() # alternatywnie nie trzeba tego defniowac tutaj a wystarzy to od razu wpisac do poniższego printa :-)

    print(f"Witaj {name.capitalize}! Miło Cię widzieć w naszym mieście: {city.capitalize}!")


print_hello_message("michał", "toruń")
print_hello_message("beata", "gdynia")


# Exercise 2 - Generate email in 4testers.pl domain using first and last name
# Jeśli chcemy aby metoda zadziałała np. lower lub capitalize musimy dodać nawiasy: ()
def get_4tester_email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


print(get_4tester_email("Janusz", "Nowak"))
print(get_4tester_email("Barbara", "Kowalska"))

#Moja funkcja
def get_make_and_name_of_the_car(car_make, car_name):
    return f"To jest mój ulubiony samochód: {car_make}-{car_name}"


print(get_make_and_name_of_the_car("BMW", "5"))
