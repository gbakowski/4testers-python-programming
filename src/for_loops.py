import uuid


def print_each_student_name_capitalized(list_of_stuednts_first_name):
    for first_name in list_of_stuednts_first_name:
        print(first_name.capitalize())


def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_numbers_from_20_to_30_multiplied_by_4():
    for number in range(20, 31):
        print(number * 4)


def print_first_ten_integers_squared():
    for integer in range(1, 101):
        print(integer * 2)


def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


def print_temperatures_in_both_scales(list_of_temps_in_celcius):
    for temp in list_of_temps_in_celcius:
        print(f"Celsius: {temp}, Farenheit: {temp * 9 / 5 + 32}")


def get_temperatures_higher_than_20_degrees(list_of_temps_in_celcius):  # filtrowanie wyników
    filtered_temperatures = []
    for temp in list_of_temps_in_celcius:
        if temp >= 20:
            filtered_temperatures.append(temp)
        return filtered_temperatures


if __name__ == '__main__':
    print_random_uuids(2)

if __name__ == '__main__':
    list_of_students = ["kate", "mark", "tosia", "miki", "jan"]
    print_each_student_name_capitalized(list_of_students)

    print_first_ten_integers_squared()

    print_numbers_from_20_to_30_multiplied_by_4()
    print_numbers_divisible_by_7(1, 30)

    temps_celsius = [10.3, 23.4, 24, 50, 21.5]
    test_temps = [0, -32 * 5 / 9]
    print_temperatures_in_both_scales(temps_celsius)
    print(get_temperatures_higher_than_20_degrees(temps_celsius))
    print(get_temperatures_higher_than_20_degrees(test_temps))
