class User:#klasa musi być napisana z dużej literki i łączymy każde kolejne słowo dużą literką
    def __init__(self, email, gender): # tutaj wpisujemy argumenty/cechy tej metody (email, gender itd) "self" = Ja
        self.email = email
        self.gender = gender
        self.usage_time = 0 #dodanie nowej lokalnej zmiennej w klasie

    def use_application(self, usage_time_in_seconds): #dodanie metody (funkcji) w ramach już utworzonej klasy
        self.usage_time += usage_time_in_seconds # modyfikacja metody, (+=) która zwiększa czas użytkownia względem wskazanego przez nas czasu/wartości
if __name__ == '__main__':
    user_kate = User("kate@example.com", "female") #inicjalizacja klasy
    user_james = User("james@example.com", "male")
    print(user_kate.email)
    print(user_james.email)
    user_kate.email = "kate@newexample.com" #zmiana emaila w Userze.
    print("This is Kate new email: ", user_kate.email)
    print(user_james.usage_time)
    print(user_kate.usage_time)
    user_kate.use_application(100)
    print(user_kate.usage_time)
    print(user_james.usage_time)