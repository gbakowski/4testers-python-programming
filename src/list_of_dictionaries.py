animal = {

    "kind": "dog",
    "aga": 2,
    "male": True

}
animal2 = {

    "kind": "cat",
    "aga": 3,
    "male": False

}

animal_kinds = ["dog", "cat", "bird"]

animals = [  # to jest lista JASON!
    {

        "kind": "dog",
        "aga": 2,
        "male": True

    },
    {

        "kind": "cat",
        "aga": 3,
        "male": False

    },
    {

        "kind": "bird",
        "age": 5,
        "male": False

    }

]
print(len(animals))
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print("The age of last animal is equal to:", last_animal_age)

print(animals[0]["male"]) #podwójna adresacja, ktora wyciąga dane z listy JASONA
animals.append(
    {
        "kind": "zebra",
        "age": 11,
        "male": False
    }
)
print(animals)